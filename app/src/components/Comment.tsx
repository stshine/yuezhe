import * as React from "react";

export interface CommentState {
    folded: boolean
}

export interface CommentProps {
    author: string,
    content: string,
    age: string
}

export default class Comment extends React.Component<CommentProps, CommentState> {
    constructor(props: CommentProps) {
        super(props);
        this.state = {
            folded: false
        }
    }

    render() {
        return (
            <div className="comment">
                <Tagline
                    folded={this.state.folded}
                    author={this.props.author}
                    age={this.props.age}
                    onClick = {this.toggle} />
                if(!this.state.folded){
                    <p className="class-content">{this.props.content}</p>
                }
            </div>
        )
    }

    toggle() {
        this.setState({folded: !this.state.folded})
    }
}

interface TaglineProps {
    folded: boolean,
    author: string,
    age: string
    onClick(e: React.MouseEvent<HTMLElement>): void
}

const Tagline = ({folded, author, age, onClick}: TaglineProps) => (
    <p className="tagline">
        <a className="toggle" onClick={onClick}>{folded ? "-" : "+"}</a>
        <a className="author">{author}</a>
        <time className="age">{age}</time>
    </p>
)