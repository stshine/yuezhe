// function commentToggle() {
    
// }
let toggles = document.getElementsByClassName("toggle");

for (let toggle of <any>toggles) {
    (toggle as HTMLElement).onclick = function(this: HTMLElement, e: MouseEvent) {
        let tagline = this.parentElement;
        let comment = tagline.parentElement;
        this.innerHTML = "[+]";
        for (let child of <any>comment.children) {
            (child as HTMLElement).style.display = "none";
        }
        tagline.style.display = "block";
        tagline.style.fontStyle = "italic";
    };
}
