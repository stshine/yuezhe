#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

use rocket::response::{NamedFile, Result};

#[get("/")]
fn index() -> Option<NamedFile> {
    NamedFile::open("dist/index.html").ok()
}

#[get("/main.js")]
fn js() -> Option<NamedFile> {
    NamedFile::open("dist/main.js").ok()
}

fn main() {
    rocket::ignite().mount("/", routes![index, js]).launch();
}
